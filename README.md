index.js
{{ range $index, $page := (where .Site.Pages "Section" "research" "") -}}

{{ $last := sub (len .Params.contributors) 1 }}
<p><small>Posted{{ if .Params.categories -}}&nbsp;in&nbsp;{{ range $index, $category := .Params.categories -}}{{ if gt $index 0 -}}, {{ end -}}<a class="stretched-link position-relative link-muted" href="{{ "/categories/" | absURL }}{{ . | urlize }}/">{{ . }}</a>{{ end -}}{{ end -}}&nbsp;on&nbsp;{{ .PublishDate.Format "January 2, 2006" }} by {{ if .Params.contributors -}}{{ range $index, $contributor := .Params.contributors }}{{ if gt $index 0 }}{{ if eq $index $last }} and {{ else }}, {{ end }}{{ end }}<a class="stretched-link position-relative" href="{{ "/contributors/" | relURL }}{{ . | urlize }}/">{{ . }}</a>{{ end -}}{{ end -}}&nbsp;&hyphen;&nbsp;<strong>{{ .ReadingTime -}}&nbsp;min read</strong></small><p>


{{- $markdownImageLinkClass := .Page.Params.imageMarkdownLinkClass | default .Page.Site.Params.imageMarkdownLinkClass -}}
{{- $markdownImageClass := .Page.Params.imageMarkdownClass | default .Page.Site.Params.imageMarkdownClass -}}
{{- $markdownImageWrapper := .Page.Params.imageMarkdownAddWrapper | default .Page.Site.Params.imageMarkdownAddWrapper -}}
{{- $image := partial "helpers/lib/image-handling/find-image-src" (dict "src" .Destination "page" .Page "getRelative" true "ignoreBundleAssets" false) -}}
{{- partial "helpers/wrapped-image" (dict "alt" .Text "altRendered" true "title" .Title "image" $image "page" .Page "noImageWrapper" (not $markdownImageWrapper) "linkClass" $markdownImageLinkClass "class" $markdownImageClass) -}}
{{- /* Remove trailing newlines */ -}}

	

{{ range $index, $page := (where .Site.Pages "Type" "searchable") -}}




figure.html
{{ $fillImage := .Scratch.Get "fillImage" }}
{{ if not $fillImage -}}
  {{ $fillImage = site.Params.fillImage }}
{{ end -}}

{{ $image := .Resources.GetMatch (printf "**%s" (index .Params.images 0)) }}
{{ if not $image -}}
  {{ $image = resources.Get (printf "%s%s" "images/" site.Params.defaultImage) }}
{{ end -}}

{{ $image = $image.Fill $fillImage }}
{{ $lqip := $image.Resize site.Params.lqipWidth -}}

{{ $imgSrc := "" -}}
{{ $imgSrcSet := slice -}}

{{ $widths := site.Params.landscapePhotoWidths -}}
{{ if gt $image.Height $image.Width -}}
  {{ $widths = site.Params.portraitPhotoWidths -}}
{{ end -}}

{{ range $widths -}}
  {{ $srcUrl := (printf "%dx" . | $image.Resize).Permalink -}}
  {{ if eq $imgSrc "" -}}{{ $imgSrc = $srcUrl -}}{{ end -}}
  {{ $imgSrcSet = $imgSrcSet | append (printf "%s %dw" $srcUrl .) -}}
{{ end -}}
{{ $imgSrcSet = (delimit $imgSrcSet ",") -}}

{{ if gt $image.Width site.Params.smallLimit -}}
  <figure class="figure">
    <img class="figure-img img-fluid lazyload blur-up" data-sizes="auto" src="{{ $lqip.Permalink }}" data-srcset="{{ $imgSrcSet }}" width="{{ $image.Width }}" height="{{ $image.Height }}" alt="{{ .Title }}">
    <noscript><img class="figure-img img-fluid" sizes="100vw" srcset="{{ $imgSrcSet }}" src="{{ $image.Permalink }}" width="{{ $image.Width }}" height="{{ $image.Height }}" alt="{{ .Title }}"></noscript>
    <!-- {{ with .Title }}<figcaption class="figure-caption">{{ . | safeHTML }}</figcaption>{{ end -}} -->
  </figure>
{{ else -}}
  <img class="img-fluid lazyload blur-up" src="{{ $lqip.Permalink }}" data-src="{{ $image.Permalink }}" width="{{ $image.Width }}" height="{{ $image.Height }}" alt="{{ .Title }}">
{{ end -}}
